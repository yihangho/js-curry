jest.dontMock('immutable');
jest.dontMock('../curry');
var curry = require('../curry');

var fn1 = function() {
  return 42;
};

var fn2 = function(a, b) {
  return a + b;
}

var fn3 = function(a, b, c) {
  return a * b * c;
}

describe('curry', function() {
  it('should be a function', function() {
    expect(typeof curry).toBe('function');
  });

  it('should detect the arity of input function', function() {
    var curriedFn1 = curry(fn1);
    var curriedFn2 = curry(fn2);

    expect(typeof curriedFn1).toBe('function');
    expect(curriedFn1()).toBe(fn1());

    expect(typeof curriedFn2).toBe('function');
    expect(curriedFn2(1)(2)).toBe(fn2(1, 2));
  });

  it('execution will not affect previous function', function() {
    var curriedFn3 = curry(fn3);
    var curriedFn3_1 = curriedFn3(1);
    var curriedFn3_2 = curriedFn3(2);
    var curriedFn3_1_2 = curriedFn3_1(2);
    var curriedFn3_2_4 = curriedFn3_2(4);

    expect(curriedFn3_1_2(3)).toBe(fn3(1, 2, 3));
    expect(curriedFn3_1_2(4)).toBe(fn3(1, 2, 4));
    expect(curriedFn3_2_4(8)).toBe(fn3(2, 4, 8));
    expect(curriedFn3_2_4(6)).toBe(fn3(2, 4, 6));
  });
});
