var Immutable = require('immutable');

module.exports = function curry(fn) {
  var workerFactory = function(currentArgs) {
    var worker = function(input) {
      var args;

      if (arguments.length > 0) {
        args = currentArgs.push(input);
      } else {
        args = currentArgs;
      }

      if (args.size === fn.length) {
        return fn.apply(undefined, args.toArray());
      } else {
        return workerFactory(args);
      }
    }

    return worker;
  }

  return workerFactory(new Immutable.List());
};
